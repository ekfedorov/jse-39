package ru.ekfedorov.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.command.AbstractProjectCommand;
import ru.ekfedorov.tm.endpoint.Project;
import ru.ekfedorov.tm.endpoint.ProjectEndpoint;
import ru.ekfedorov.tm.endpoint.Session;
import ru.ekfedorov.tm.enumerated.Sort;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

public final class ProjectListCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Show all project.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-list";
    }

    @SneakyThrows
    @Override
    public void execute() {
        if (bootstrap == null) throw new NullObjectException();
        @Nullable final Session session = bootstrap.getSession();
        if (endpointLocator == null) throw new NullObjectException();
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @Nullable final String sort = TerminalUtil.nextLine();
        @NotNull final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        @Nullable List<Project> projects;
        if (isEmpty(sort)) projects = projectEndpoint.findProjectAll(session);
        else projects = projectEndpoint.findProjectAllWithComparator(session, sort);
        if (projects.isEmpty()) {
            System.out.println("--- There are no projects ---");
            return;
        }
        int index = 1;
        for (@NotNull final Project project : projects) {
            System.out.println(index + ". " + toString(project));
            index++;
        }
        System.out.println();
    }

    @NotNull
    public String toString(@NotNull final Project project) {
        return project.getId() + ": " + project.getName() + " | " + project.getStatus();
    }

}

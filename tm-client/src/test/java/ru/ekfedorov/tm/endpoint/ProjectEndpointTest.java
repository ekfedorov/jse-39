package ru.ekfedorov.tm.endpoint;

import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.ekfedorov.tm.api.endpoint.EndpointLocator;
import ru.ekfedorov.tm.bootstrap.Bootstrap;
import ru.ekfedorov.tm.marker.IntegrationCategory;

import java.util.List;

public class ProjectEndpointTest {

    final EndpointLocator endpointLocator = new Bootstrap();

    private Session session;

    private Session sessionAdmin;

    @Before
    @SneakyThrows
    public void before() {
        session = endpointLocator.getSessionEndpoint().openSession("test", "test");
        sessionAdmin = endpointLocator.getSessionEndpoint().openSession("admin", "admin");
        endpointLocator.getProjectEndpoint().clearBySessionProject(session);
        endpointLocator.getProjectEndpoint().clearBySessionProject(sessionAdmin);
    }

    @After
    @SneakyThrows
    public void after() {
        endpointLocator.getProjectEndpoint().clearBySessionProject(session);
        endpointLocator.getProjectEndpoint().clearBySessionProject(sessionAdmin);
        endpointLocator.getSessionEndpoint().closeSession(session);
        endpointLocator.getSessionEndpoint().closeSession(sessionAdmin);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void addProjectTest() {
        final String projectName = "nameTest";
        final String projectDescription = "nameTest";
        endpointLocator.getProjectEndpoint().addProject(session, projectName, projectDescription);
        final Project project = endpointLocator.getProjectEndpoint().findProjectOneByName(session, projectName);
        Assert.assertNotNull(project);
        Assert.assertEquals(projectName, project.getName());
        Assert.assertEquals(projectDescription, project.getDescription());
    }


    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void changeProjectStatusByIdTest() {
        final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        final Project project = projectEndpoint.addProject(session, "projectTest", "descrTest");
        projectEndpoint.changeProjectStatusById(session, project.getId(), Status.COMPLETE);
        final Project projectChanged = projectEndpoint.findProjectOneById(session, project.getId());
        Assert.assertEquals(Status.COMPLETE, projectChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void changeProjectStatusByIndexTest() {
        final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        final Project project = projectEndpoint.addProject(session, "projectTest", "descrTest");
        final List<Project> projects = projectEndpoint.findProjectAll(session);

        int pos = 0;
        for(Project t : projects) {
            if(project.getId().equals(t.getId())) break;
            pos++;
        }

        projectEndpoint.changeProjectStatusByIndex(session, pos, Status.COMPLETE);
        final Project projectChanged = projectEndpoint.findProjectOneById(session, project.getId());
        Assert.assertEquals(Status.COMPLETE, projectChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void changeProjectStatusByNameTest() {
        final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        final Project project = projectEndpoint.addProject(session, "projectTest", "descrTest");
        projectEndpoint.changeProjectStatusByName(session, project.getName(), Status.COMPLETE);
        final Project projectChanged = projectEndpoint.findProjectOneById(session, project.getId());
        Assert.assertEquals(Status.COMPLETE, projectChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findAllProjectTest() {
        final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        projectEndpoint.addProject(session, "projectTest", "descrTest");
        projectEndpoint.addProject(session, "projectTest2", "descrTest");
        projectEndpoint.addProject(session, "projectTest3", "descrTest");
        Assert.assertEquals(3, projectEndpoint.findProjectAll(session).size());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findProjectAllWithComparatorTest() {
        final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        projectEndpoint.addProject(session, "cprojectTest", "descrTest");
        projectEndpoint.addProject(session, "aprojectTest2", "descrTest");
        projectEndpoint.addProject(session, "bprojectTest3", "descrTest");
        final List<Project> projects = projectEndpoint.findProjectAllWithComparator(session, "NAME");
        Assert.assertEquals("aprojectTest2", projects.get(0).getName());
        Assert.assertEquals("bprojectTest3", projects.get(1).getName());
        Assert.assertEquals("cprojectTest", projects.get(2).getName());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findProjectOneByIdTest() {
        final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        final Project project = projectEndpoint.addProject(session, "projectTest", "descrTest");
        Assert.assertNotNull(projectEndpoint.findProjectOneById(session, project.getId()));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findProjectOneByIndexTest() {
        final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        projectEndpoint.addProject(session, "projectTest0", "descrTest");
        projectEndpoint.addProject(session, "projectTest", "descrTest");
        projectEndpoint.addProject(session, "projectTest3", "descrTest");
        final Project project = projectEndpoint.findProjectAll(session).get(1);
        final Project projectFind = projectEndpoint.findProjectOneByIndex(session, 1);
        Assert.assertNotNull(projectFind);
        Assert.assertEquals(project.getId(), projectFind.getId());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findProjectOneByNameTest() {
        final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        projectEndpoint.addProject(session, "projectTest0", "descrTest");
        final Project project = projectEndpoint.addProject(session, "projectTest", "descrTest");
        projectEndpoint.addProject(session, "projectTest3", "descrTest");
        final Project projectFind = projectEndpoint.findProjectOneByName(session, "projectTest");
        Assert.assertNotNull(projectFind);
        Assert.assertEquals(project.getId(), projectFind.getId());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void finishProjectByIdTest() {
        final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        final Project project = projectEndpoint.addProject(session, "projectTest", "descrTest");
        projectEndpoint.finishProjectById(session, project.getId());
        final Project projectChanged = projectEndpoint.findProjectOneById(session, project.getId());
        Assert.assertEquals(Status.COMPLETE, projectChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void finishProjectByIndexTest() {
        final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        projectEndpoint.addProject(session, "projectTest0", "descrTest");
        projectEndpoint.addProject(session, "projectTest", "descrTest");
        projectEndpoint.addProject(session, "projectTest3", "descrTest");
        final Project project = projectEndpoint.findProjectAll(session).get(1);
        projectEndpoint.finishProjectByIndex(session, 1);
        final Project projectChanged = projectEndpoint.findProjectOneById(session, project.getId());
        Assert.assertEquals(Status.COMPLETE, projectChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void finishProjectByNameTest() {
        final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        projectEndpoint.addProject(session, "projectTest0", "descrTest");
        final Project project = projectEndpoint.addProject(session, "projectTest", "descrTest");
        projectEndpoint.addProject(session, "projectTest3", "descrTest");
        projectEndpoint.finishProjectByName(session, "projectTest");
        final Project projectChanged = projectEndpoint.findProjectOneByName(session, project.getName());
        Assert.assertEquals(Status.COMPLETE, projectChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeProjectTest() {
        final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        projectEndpoint.addProject(session, "projectTest0", "descrTest");
        final Project project = projectEndpoint.addProject(session, "projectTest", "descrTest");
        projectEndpoint.addProject(session, "projectTest3", "descrTest");
        Assert.assertNotNull(projectEndpoint.findProjectOneById(session, project.getId()));
        projectEndpoint.removeProject(session, project);
        Assert.assertNull(projectEndpoint.findProjectOneById(session, project.getId()));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeProjectOneByIdTest() {
        final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        projectEndpoint.addProject(session, "projectTest0", "descrTest");
        final Project project = projectEndpoint.addProject(session, "projectTest", "descrTest");
        projectEndpoint.addProject(session, "projectTest3", "descrTest");
        Assert.assertNotNull(projectEndpoint.findProjectOneById(session, project.getId()));
        projectEndpoint.removeProjectOneById(session, project.getId());
        Assert.assertNull(projectEndpoint.findProjectOneById(session, project.getId()));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeProjectOneByIndexTest() {
        final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        projectEndpoint.addProject(session, "projectTest0", "descrTest");
        projectEndpoint.addProject(session, "projectTest", "descrTest");
        projectEndpoint.addProject(session, "projectTest3", "descrTest");
        final Project project = projectEndpoint.findProjectAll(session).get(1);
        Assert.assertNotNull(projectEndpoint.findProjectOneById(session, project.getId()));
        projectEndpoint.removeProjectOneByIndex(session, 1);
        Assert.assertNull(projectEndpoint.findProjectOneById(session, project.getId()));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeProjectOneByNameTest() {
        final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        projectEndpoint.addProject(session, "projectTest0", "descrTest");
        final Project project = projectEndpoint.addProject(session, "projectTest", "descrTest");
        projectEndpoint.addProject(session, "projectTest3", "descrTest");
        Assert.assertNotNull(projectEndpoint.findProjectOneById(session, project.getId()));
        projectEndpoint.removeProjectOneByName(session, "projectTest");
        Assert.assertNull(projectEndpoint.findProjectOneById(session, project.getId()));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void startProjectByIdTest() {
        final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        final Project project = projectEndpoint.addProject(session, "projectTest", "descrTest");
        projectEndpoint.startProjectById(session, project.getId());
        final Project projectChanged = projectEndpoint.findProjectOneById(session, project.getId());
        Assert.assertEquals(Status.IN_PROGRESS, projectChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void startProjectByIndexTest() {
        final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        projectEndpoint.addProject(session, "projectTest0", "descrTest");
        projectEndpoint.addProject(session, "projectTest", "descrTest");
        projectEndpoint.addProject(session, "projectTest3", "descrTest");
        final Project project = projectEndpoint.findProjectAll(session).get(1);
        projectEndpoint.startProjectByIndex(session, 1);
        final Project projectChanged = projectEndpoint.findProjectOneById(session, project.getId());
        Assert.assertEquals(Status.IN_PROGRESS, projectChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void startProjectByNameTest() {
        final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        projectEndpoint.addProject(session, "projectTest0", "descrTest");
        final Project project = projectEndpoint.addProject(session, "projectTest", "descrTest");
        projectEndpoint.addProject(session, "projectTest3", "descrTest");
        projectEndpoint.startProjectByName(session, "projectTest");
        final Project projectChanged = projectEndpoint.findProjectOneByName(session, project.getName());
        Assert.assertEquals(Status.IN_PROGRESS, projectChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void updateProjectByIdTest() {
        final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        final String newName = "projectTestNew";
        final String newDescription = "descrTestNew";
        final Project project = projectEndpoint.addProject(session, "projectTest", "descrTest");
        projectEndpoint.updateProjectById(session, project.getId(), newName, newDescription);
        final Project projectUpdate = projectEndpoint.findProjectOneById(session, project.getId());
        Assert.assertEquals(newName, projectUpdate.getName());
        Assert.assertEquals(newDescription, projectUpdate.getDescription());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void updateProjectByIndexTest() {
        final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        final String newName = "projectTestNew";
        final String newDescription = "descrTestNew";
        final Project project = projectEndpoint.addProject(session, "projectTest", "descrTest");
        projectEndpoint.updateProjectByIndex(session, 0, newName, newDescription);
        final Project projectUpdate = projectEndpoint.findProjectOneById(session, project.getId());
        Assert.assertEquals(newName, projectUpdate.getName());
        Assert.assertEquals(newDescription, projectUpdate.getDescription());
    }

}

